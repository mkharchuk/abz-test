<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EmployeeController@treeView');
Route::get('/tree', 'EmployeeController@rootData')->name('tree.rootData');
Route::get('/tree/child', 'EmployeeController@childData')->name('tree.childData');
Route::group(['middleware' => 'auth'], function() {
    Route::get('/employee/list', 'EmployeeController@employeeList')->name('employee.list');
    Route::get('/getBoss', 'EmployeeController@getParent')->name('employee.create.getBoss');
    Route::get('/employee/search', 'EmployeeController@search')->name('employee.search');
    Route::post('/employee/delete', 'EmployeeController@delete')->name('employee.delete');
    Route::resource('/employee', 'EmployeeController');
});

Auth::routes();