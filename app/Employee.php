<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Employee extends Model
{
    //
    protected  $fillable = ['name', 'position_id', 'parent_id', 'start_date', 'salary', 'created_by', 'modified_by'];

    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

}
