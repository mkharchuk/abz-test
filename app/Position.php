<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //relationship
    public function employees()
    {
        return $this->hasMany('App\Employees');
    }

}
