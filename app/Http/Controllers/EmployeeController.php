<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{

    public function index()
    {

        return view('employees.index');

    }

    public function employeeList()
    {
        $collection = [];
        $nodes = DB::table('positions')
            ->join('employees', 'positions.id', '=', 'employees.position_id')
            ->select('employees.name', 'employees.id', 'positions.position_name', 'employees.start_date','employees.salary')->get();
//        $collection = collect($nodes);
//        $collection->toJson();
        foreach ($nodes as $key => $node){
            $collection['data'][$key] = collect([$node->name, $node->position_name, $node->start_date, $node->salary, $node->id]);
        }
//        dd($collection);
        return $collection;
    }

    public function create()
    {
        return view('employees.create', [
            'employee' => '',
            'positions' => Position::all(),
            'employees' => Employee::where('parent_id', '0')->get(),
            'delimiter'  => ''
        ]);
    }

    public function store(Request $request)
    {
        Employee::create($request->all());
        return redirect()->route('employee.index');
    }

    public function delete(Request $request)
    {
        Employee::where('id', $request->id)->delete();
        return redirect()->route('employee.index');
    }

    public function treeView()
    {
        return view('tree');
    }

    public function rootData()
    {

        return $this->dataBuilder(0);
    }

    public function childData(Request $request)
    {
        return $this->dataBuilder($request->id);
    }

    public function chekID($id)
    {
        return Employee::where('parent_id', $id)->count() > 0 ? true : false;
    }

    public function childrenCount($id)
    {
        $count = Employee::with('children')->where('parent_id', $id)->count();
        return  $count > 0 ?  '   || Підлеглих - ' . $count : '';
    }


    public function dataBuilder($id)
    {
        $nodes = [];
        $employees = Employee::with('position')->where('parent_id', $id)->get();

        foreach ($employees as $key => $employee){
            $nodes[$key]['id'] = $employee->id;
            $nodes[$key]['parent'] = $employee->parent_id == 0 ? '#' : $employee->parent_id;
            $nodes[$key]['text'] = $employee->name . '   ||   ' . $employee->position->position_name . $this->childrenCount($employee->id);
            $nodes[$key]['icon'] = 'fa fa-male';
            $nodes[$key]['state'] = [
                'disabled' => false,
                'opened'   => false,
            ];
            $nodes[$key]['children'] = $this->chekID($employee->id);
            $nodes[$key]['li_attr'] = [];
            $nodes[$key]['a_attr'] = [];
        }

        return response()->json($nodes);
    }

    public function getParent(Request $request)
    {
        $data = Employee::select('id', 'name')
            ->where('position_id', '=', $request->id - 1)
            ->orderBy('name','asc')
            ->get();
        return response()->json($data);
    }

}
