<label for="">ПІБ</label>
<input type="text" class="form-control" name="name" placeholder="Прізвище ім'я"  required>

<label for="">Посада</label>
<select class="form-control position" name="position_id">
    @foreach($positions as $position)
            <option value="{{$position->id}}">{{$position->position_name}}</option>
    @endforeach
</select>

<label for="" id="load">Начальник</label>
<select class="form-control text-left parent" name="parent_id">
    <option value="" selected="true">-------- Начальник --------</option>
</select>
<label for="">Дата початку роботи</label>
<input type="date" class="form-control" name="start_date" placeholder="Дата" required>

<label for="">Зарплата</label>
<input type="text" class="form-control" name="salary" placeholder="Зарплата"  required>

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">