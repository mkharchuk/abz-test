@extends('layouts.app')

@section('content')

    <div>
        <a href="{{route('employee.create')}}" class="btn btn-primary pull-left"><i class="fa fa-plus-square-o"></i>Добавити робітника</a>
    </div>

    <table class="table table-striped" id="qq">
        <thead>
        <tr>
            <th>ПІБ</th>
            <th>Посада</th>
            <th>Дата прийому</th>
            <th>Зарплата</th>
            <th class="text-center">Дія</th>
        </tr>


        </thead>
    </table>
@endsection


@section('script')
    <script src="{{ asset('js/sort.js') }}"></script>
@endsection