@extends('layouts.app')

@section('content')

    <div class="container" style="width: 30%">

        <form class="form-horizontal" action="{{route('employee.store')}}" method="post">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('employees.partials.form')

        </form>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/dropDownChange.js') }}"></script>
@endsection