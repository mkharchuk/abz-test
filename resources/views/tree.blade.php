@extends('layouts.app')

@section('content')

    <div class="container">
        <div id="jstree"></div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $('#jstree').jstree({
            'core': {
                'data': {
                    'url' : function (node) {
                        return node.id === '#' ?
                            '{!! route('tree.rootData') !!}' : '{!! route('tree.childData') !!}';
                    },
                    'data': function (node) {
                        return {'id': node.id};
                    },
                },

                'error': function (data) {
                    $('#jstree').html('<p>We had an error...</p>');
                }
            }
        });
    </script>
@endsection
