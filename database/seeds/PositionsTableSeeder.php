<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('positions')->insert([
            'position_name' => "Директор",
        ]);
        DB::table('positions')->insert([
            'position_name' => "Зам директора",
        ]);
        DB::table('positions')->insert([
            'position_name' => "Начальник відділу",
        ]);
        DB::table('positions')->insert([
            'position_name' => "Старший менеджер",
        ]);
        DB::table('positions')->insert([
            'position_name' => "Менеджер",
        ]);
    }
}
