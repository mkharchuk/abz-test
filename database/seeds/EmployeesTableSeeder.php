<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = 50000;
        factory(App\Employee::class)->create(['position_id' => 1])->make();

        factory(App\Employee::class, $count * 0.05)->create([ //99
            'position_id' => 2,
            'parent_id' => 1
        ])->make();



        for ($i = $count * 0.05; $i <=$count; $i++)//50000
        {
            if ($i >= $count * 0.05 + 1 && $i <= $count * 0.1) //101 1000
            {
                factory(App\Employee::class)->create([
                    'position_id' => 3,
                    'parent_id' => mt_rand(2, $count * 0.05) //2 100
                ])->make();
            }

            if ($i >= $count * 0.1 + 1 && $i <= $count * 0.25) //1001 2000
            {
                factory(App\Employee::class)->create([
                    'position_id' => 4,
                    'parent_id' => mt_rand($count * 0.05 + 1, $count * 0.1) // 101 1000
                ])->make();
            }

            if ($i >= $count * 0.25 + 1) //2001 50000
            {
                factory(App\Employee::class)->create([
                    'position_id' => 5,
                    'parent_id' => mt_rand($count * 0.1 + 1, $count * 0.25) // 1001 10000
                ])->make();
            }

        }
    }
}
