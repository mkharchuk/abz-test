$(document).ready(function () {
    $('#load').append('<div id="ajaxBusy"><p><img src="/25.gif"></p></div>');


    $(document).on('change', '.position', function () {
        $('#ajaxBusy').show();

        var position_id = $(this).val();

        var div = $(this).parent();
        var op = " ";
        $.ajax({
            'type': 'GET',
            'url': '/getBoss',
            'data': {
                'id': position_id
            },
            success: function (data) {

                if (data.length > 0){
                    op+='<option value="0" selected disabled>-------- Виберіть начальника --------</option>';

                    for (var i = 0; i<data.length; i++){
                        op+='<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                } else {

                    op+='<option value="0" selected="true">-------- Начальник --------</option>'
                }

                $('#ajaxBusy').hide();

                div.find('.parent').html(" ");
                div.find('.parent').append(op);
            }
        });

    });

});
