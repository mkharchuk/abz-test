$(document).ready( function () {

    $('#qq').DataTable( {
        "ajax": "employee/list",
        "deferRender": true,
        "columnDefs": [ {
            "targets": 4,
            "orderable": false,
            "searchable": false,
            "createdCell": function (td, cellData, rowData, row, col) {
                $(td).html('<form onsubmit="if(confirm(\'Видалити?\')){ return true } else { return false }" class="form-horizontal" action="employee/delete" method="post">' +
                    '<button class="btn" type="submit"><i class="fa fa-trash-o"></i></button>' +
                    '<input type="hidden" name="id" value="'+ cellData +'">' +
                    '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
                '</form>');

            }
        } ],
        "language": {
            "search": "Пошук ",
            "info": "Показано _START_ до _END_ з _TOTAL_ записів",
            "lengthMenu": "Відобразити _MENU_ записів",
            "loadingRecords": "Зачекайте  - дані завантажуються...",
            "zeroRecords": "Немає даних для відображення",
            "paginate": {
                "first": "Перша сторінка",
                "last": "Остання сторінка",
                "next": "Наступна",
                "previous": "Попередня"
            }
        },
        "pageLength": 50,
        "columns": [
            { "data": "0" },
            { "data": "1" },
            { "data": "2" },
            { "data": "3" },
            { "data": "4" }
        ]

    } );
} );